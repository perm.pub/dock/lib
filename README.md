<!-- copybreak off -->

GitLab CI/CD Component Library for perm.pub Related Container Images
====================================================================

This repository contains
[GitLab CI/CD components](https://docs.gitlab.com/ee/ci/components/)
for building OCI container images.


`build-image`
-------------

The [build-image](./templates/build-image.yml) component can be used by a GitLab
repository to automatically build and deploy an OCI image to registry.gitlab.com.

## Steps to Use `build-image`

### Step 1

Create a repository on GitLab dedicated to building and deploying one OCI image.

### Step 2

Create a top-level executable file named `build-image` that will build an OCI image.
The built OCI image must be tagged with the first command-line parameter,
which will be the full image name with image tag.

### Step 3

Create a top-level file named `.gitlab-ci.yml` with the following contents:

```
include:
  - component: gitlab.com/perm.pub/dock/lib/build-image@main
```

Replace `main` with one of the [version tags of perm.pub/dock/lib](https://gitlab.com/perm.pub/dock/lib/-/tags).

<!-- copybreak on -->

### Step 4

To initiate an image build, (create and) push a branch in the OCI image-building repository.
The branch name should start with the prefix `ci/` and end with the suffix `/build-image`.
The OCI repository path will be the GitLab repository path plus an extra path between
the prefix and suffix.
If the branch is named `ci/build-image`, then no extra path is added.
